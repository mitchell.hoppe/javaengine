//package experiments.spring;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import org.springframework.stereotype.Service;
//
//@Service
//public class GameService {
//
//	private List<GameState> gameList = new ArrayList<>(Arrays.asList(new GameState(800, "Escape", "Bob"),
//			(new GameState(500, "Chess", "Walker"))));
//
//	public List<GameState> getAllGamesStates() {
//		return gameList;
//	}
//	
//	public String asString() {
//		return gameList.toString();
//	}
//	
//	public GameState getGameState(String id) {
//		return gameList.stream().filter(gs -> gs.getName().equals(id)).findFirst().get();
//	}
//	
//	public void addGameState(GameState gs) {
//		gameList.add(gs);
//	}
//}
