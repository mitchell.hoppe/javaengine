//package experiments.spring;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//public class GameStateController {
//
//	@Autowired
//	private GameService gameService;
//	
//	@RequestMapping("/hello")
//	public String sayHi() {
//		return "Hello";
//	}
//	
//	@RequestMapping("/gamestates")
//	public List<GameState> getGameStates(){
//		return gameService.getAllGamesStates();
//	}
//	
//	@RequestMapping("/gamestring")
//	public String asString() {
//		return gameService.asString();
//	}
//	
//	@RequestMapping("/gamestates/{id}")
//	public GameState getGameState(@PathVariable String id) {
//		return gameService.getGameState(id);
//	}
//	
//	@RequestMapping(method=RequestMethod.POST, value="/gamestates")
//	public void addGameState(@RequestBody GameState gs) {
//		gameService.addGameState(gs);
//	}
//}
