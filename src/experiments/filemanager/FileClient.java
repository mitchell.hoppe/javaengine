package experiments.filemanager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;

import projectx.engine.Handler;
import projectx.engine.io.IO;

public class FileClient {

	int port = 4040;

	String address = "10.24.226.239";
	Socket socket;
	static Handler handler;
	File worldFile;

	public FileClient() {
		init();
	}

	private void init() {
		try {
			socket = new Socket();
			SocketAddress socketAddress = new InetSocketAddress(InetAddress.getByName(getAddress()), getPort());
			socket.connect(socketAddress);
			if (socket != null) {
//				IO.println(socket.toString());
				run();
			}

		} catch (IOException ioe) {
			IO.printlnErr(" " + ioe + "\nUnable to  connect to '" + getAddress() + "' server on port '" + getPort()
					+ "' \nPlease make sure that the server is running.");
		}
	}

	public String getAddress() {
		return address;
	}

	public int getPort() {
		return port;
	}

	public void run() {
		// Starts a new thread for the client to stay active to communicate with the
		// server and it's clients
		Thread t = new Thread(new ServerScanner(socket));
		t.start();
	}

	/**
	 * Ends the client
	 */
	public void end() {
		System.exit(0);
	}

	/**
	 * 
	 * @author Kenneth Lange
	 *
	 *         This class is directly used by the Client class for receiving the
	 *         input stream back from the server that it connects to.
	 */
	private class ServerScanner implements Runnable {
		Socket socket;

		public ServerScanner(Socket socket) {
			this.socket = socket; // Used to make the socket transferable from one class to the next.

		}

		@Override
		public void run() {
			try {

				while (socket != null && !socket.isClosed()) {

					BufferedInputStream bin = new BufferedInputStream(socket.getInputStream());

					if (bin.available() > 0) {
						worldFile = new File(filenameGen());
						worldFile.createNewFile();
						BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(worldFile));
						int count;
						byte[] buffer = new byte[50000];
						while ((count = bin.read(buffer)) != -1) {
							bout.write(buffer, 0, count);
							// IO.println(new String(buffer));
						}
						IO.println("file received");
						bout.close();

					}

				}
			} catch (IOException e) {
				IO.printStackTrace(e);
			}
		}

	}

	/**
	 * Helper method to generate a unique world file based on current time
	 * 
	 * @return
	 */
	private String filenameGen() {
		String filename = "save" + java.time.LocalDateTime.now();
		filename = filename.replace(":", "");
		filename = filename.replace(".", "");
		filename = filename.replace("-", "");
		filename += ".txt";
		return filename;
	}

	/**
	 * sends a key to the server asking to receive the world file
	 */
	public void sendReq() {
		sendFile("get.txt");
	}
	
	/**
	 * method to shutdown server from client side
	 */
	public void shutdown() {
		sendFile("server_shutdown.txt");
	}

	public void sendFile(String s) {
		try {
			File file = new File(s);
			BufferedInputStream bin = new BufferedInputStream(new FileInputStream(file));
			BufferedOutputStream bout = new BufferedOutputStream(socket.getOutputStream());
			int count;
			byte[] buffer = new byte[50000];
			while ((count = bin.read(buffer)) != -1) {
				bout.write(buffer, 0, count);
				IO.println(new String(buffer));
			}
			IO.println("file " + s + " sent");
			bin.close();
		} catch (IOException e) {
			IO.printStackTrace(e);
		}
	}

	

	public static void main(String[] args) {
		FileClient f = new FileClient();
		Scanner s = new Scanner(System.in);
		IO.println("Enter a command: ");
		String next = s.next();
		while (!next.equals("end")) {
			if (next.equals("send")) {
				IO.println("Enter filename: ");
				next = s.next();
				f.sendFile(next);
			} else if (next.equals("get")) {
				f.sendReq();
			} else if (next.equals("end"))
				break;
			else if (next.equals("shutdown")) {
				f.shutdown();
			} else
				IO.println("try again");
			next = s.next();
		}
		s.close();

	}
}
