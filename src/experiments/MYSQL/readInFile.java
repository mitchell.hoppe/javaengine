package experiments.MYSQL;
import java.io.*;

public class readInFile {
	public static void main(String args[]) throws IOException {
	FileReader in = null; 
	FileWriter out = null; 
	
	//String x;
		try {
			
			in = new FileReader("input.txt");
			out = new FileWriter("output.txt"); // this will be changed to be more modular and
			int c; 
			
			while((c=in.read())!=-1) {
				out.write(c);
			}
		} finally {
				if(in !=null) {
					in.close();
				} 
				if(out !=null) {
					out.close();
				}
			
		}
	}
}



