package experiments.MYSQL; 
import java.sql.*; 
public class JDBC_experiment00 {
	// JDBC Driver name and DB URL
	static String Driver = "jdbc:mysql://hostname:3306/309_projectx"; // this will be updated once the server connection name is figured out 
	static String URL = "proj309-sb-06.misc.iastate.edu"; // this will be updated once the server connection name is figured out 
	static  final String JDBC_DRIVER = Driver; 
	static final String DB_URL = URL;
	
	// creadentials  // I am not sure how this would be done with a local host
	static final String USER = "team6"; 
	static final String PASS = "SB_6projectuser"; 
	
	public static void main(String[] args) throws SQLException {
		Connection conn = null; 
		Statement stmt = null; 
		
		// try and catch statements ... 
		try {
			System.out.println("Loading driver...");


			
			Class.forName("com.mysql.jdbc.Driver"); 
			System.out.println("driver loaded");
			
			System.out.print("connecting to the database");
			
			// open a connection 
			conn =DriverManager.getConnection(DB_URL, USER, PASS);
			//System.out.println("Connecting to database...");
		
		// execute a query. 
			System.out.println("Creating a statement...");
			stmt = conn.createStatement();
			String sql; 
			System.out.println("");
			sql = "CREATE TABLE Employees"; // creating a custom query
			ResultSet rs = stmt.executeQuery(sql); // executing the query. 
			System.out.print("TAble created");

			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		} finally {
			
				try {
					if(stmt !=null)
						stmt.close(); 
				}catch(SQLException se2) {
					
				} // do nothing 
				try {
					if(conn != null)
						conn.close(); 
						
				}catch(SQLException se) {
					se.printStackTrace();
				} // end finally try
		    } // end try 
		System.out.println("Exiting");
  } // end main 
}