package experiments.MYSQL; 
import java.sql.*; 

public class JDBC_localDB {

	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver"; 
	static final String DB_URL = "jdbc:mysql://proj309-sb-06.misc.iastate.edu:3306/309_projx?serverTimezone=UTCuseUnicode=true&characterEncoding=UTF-8"; 
	
	// creadentials  // I am not sure how this would be done with a local host
	static final String USER = "team06"; 
	static final String PASS = "SB_6projectuser"; 
	
	public static void main(String[] args) throws SQLException {
		Connection conn = null; 
		Statement stmt = null; 
		
		// try and catch statements ... 
		try {
			
			Class.forName("com.mysql.jdbc.Driver"); 
			System.out.printf("attempting to connect to %s \n",DB_URL);
			
				try {
					// open a connection 
					conn =DriverManager.getConnection(DB_URL, USER, PASS);
					System.out.println("Successfully connected to the database");
				} catch(Exception e) {
					System.out.println("cannot connect properly \n exiting");
					System.exit(0);
			}
			
			
			// test cases for query(s). 
			
			stmt = conn.createStatement();
			
			//----------------------------------------------------------------------Test cases-------------------------------------------
		
						/*
							//creating database      // getting an error because of the user access will try and give all access necessary when i ssh @Alain 10-30-18
							 
							System.out.println("Creating database");
							String c_db = "CREATE DATABASE TestDB_00";
							stmt.execute(c_db); 
							System.out.println("database created successfully");
						*/
			
			
			
			
//			//creating a table 
//			System.out.println("Creating table...");
//			
//			 String sql1 = "CREATE TABLE byte_array_Table " +
//	                   "(byte_value char(9) not NULL)"; 
//			
//			//String table = "CREATE TABLE byte_table";
//			stmt.executeUpdate(sql1);
//			System.out.println("table created");
//			
			// deleting tables 
			
			System.out.println("Deleting table....");
			String delT ="DROP TABLE byte_array_Table"; 
			stmt.executeUpdate(delT);
		System.out.println("Table successfully dropped ");
		
			
			//deleting table 
			//System.out.println("Deleting table");
			//String rem = "DELETE TABLE byte_array_table";
			//stmt.executeQuery(rem); 
			//String sql ="SELECT* from Entity_inventory"; 
			//stmt.executeUpdate(sql); 
			//stmt.executeQuery(sql1);
			//System.out.println("database created successfully");
		
		
			stmt.close();
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		} finally {
			
				try {
					if(stmt !=null)
						stmt.close(); 
				}catch(SQLException se2) {
					
				} // do nothing 
				try {
					if(conn != null)
						conn.close(); 
						
				}catch(SQLException se) {
					se.printStackTrace();
				} // end finally try
		    } // end try 
		System.out.println("Exiting");
  } // end main 
	
	
	
	
}