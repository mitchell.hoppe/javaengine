package experiments;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

import javax.swing.text.AbstractDocument;

public class Window extends JFrame implements ActionListener {

    // GUI stuff
    private JTextArea  enteredText = new JTextArea(10, 32);
    private JTextField typedText   = new JTextField(32);

    public Window() {
        addWindowListener(
            new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                	
                }
            }
        );
        
        Font font1 = new Font("Courier", Font.BOLD, 42);
         
        typedText.setFont(font1);
        enteredText.setFont(font1);
        
        DocumentFilter filter = new LowercaseDocumentFilter();
        ((AbstractDocument) enteredText.getDocument()).setDocumentFilter(filter);
        ((AbstractDocument) typedText.getDocument()).setDocumentFilter(filter);


        // create GUI stuff
        enteredText.setEditable(false);
        enteredText.setBackground(Color.LIGHT_GRAY);
        typedText.addActionListener(this);

        Container content = getContentPane();
        content.add(new JScrollPane(enteredText), BorderLayout.CENTER);
        content.add(typedText, BorderLayout.SOUTH);


        // display the window, with focus on typing box
        setTitle("Terminal");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        typedText.requestFocusInWindow();
        setVisible(true);

    }
    
    public void run() {}
	
	public void stop() {
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}
	
	public void returnCommand() {}
	
	public void update() {}
	
	public void render(Graphics g) {}
	
	public void print(String s) {enteredText.append(s + "\n");}
	
	public String toString() {return "";}

    // process TextField after user hits Enter
    public void actionPerformed(ActionEvent e) {
    	String text = typedText.getText();
    	
    	enteredText.append(text + "\n");
    	typedText.selectAll();
        typedText.requestFocusInWindow();
        typedText.setText("");
    }

    public static void main(String[] args)  {
    	Window client = new Window();
    }
    
    class LowercaseDocumentFilter extends DocumentFilter {
        public void insertString(DocumentFilter.FilterBypass fb, int offset,
                String text, AttributeSet attr) throws BadLocationException {

            fb.insertString(offset, text.toLowerCase(), attr);
        }

        public void replace(DocumentFilter.FilterBypass fb, int offset, int length,
                String text, AttributeSet attrs) throws BadLocationException {

            fb.replace(offset, length, text.toLowerCase(), attrs);
        }
    }
}