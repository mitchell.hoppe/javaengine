package experiments;

public class Demo extends MyClass{

    public Demo() {
        System.out.println("Hi!");
    }

    public static void main(String[] args) throws Exception {
        Class clazz = Class.forName("experiments.Demo");
        MyClass demo = (MyClass) clazz.newInstance();
    }
    
    public void runMethod() {
    	System.out.println("Method run.");
    }
}