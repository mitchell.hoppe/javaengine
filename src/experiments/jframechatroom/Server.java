//package experiments.jframechatroom;
//
//import java.awt.BorderLayout;
//import java.awt.EventQueue;
//import java.awt.Window;
//
//import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.border.EmptyBorder;
//import javax.swing.JTextField;
//import javax.swing.JLabel;
//import javax.swing.JOptionPane;
//import javax.swing.JButton;
//import java.awt.event.ActionListener;
//import java.awt.event.ActionEvent;
//
//public class Server extends JFrame {
//
//	private JPanel contentPane;
//	private JTextField name1;
//	private JTextField name2;
//
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Server frame = new Server();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	public Server() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setBounds(100, 100, 450, 300);
//		contentPane = new JPanel();
//		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//		setContentPane(contentPane);
//		contentPane.setLayout(null);
//		
//		name1 = new JTextField();
//		name1.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				createRoom();
//			}
//		});
//		name1.setBounds(83, 64, 254, 43);
//		contentPane.add(name1);
//		name1.setColumns(10);
//		
//		name2 = new JTextField();
//		name2.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				createRoom();
//			}
//		});
//		name2.setBounds(83, 156, 254, 43);
//		contentPane.add(name2);
//		name2.setColumns(10);
//		
//		JLabel lblstUsername = new JLabel("1st username");
//		lblstUsername.setBounds(83, 39, 254, 14);
//		contentPane.add(lblstUsername);
//		
//		JLabel lblndUsername = new JLabel("2nd username");
//		lblndUsername.setBounds(83, 131, 254, 14);
//		contentPane.add(lblndUsername);
//		
//		JButton btnNewButton = new JButton("JOIN CHAT");
//		btnNewButton.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				createRoom();
//			}
//		});
//		btnNewButton.setBounds(119, 210, 198, 41);
//		contentPane.add(btnNewButton);
//	}
//	
//	public void createRoom() {
//		String p1, p2;
//		
//		p1 = name1.getText();
//		p2 = name2.getText();
//		
//		if (p1.equals("") || p2.equals("")) {
//			JOptionPane.showMessageDialog(null, "Please enter a valid username!");
//			return;
//		}
//		
//		Window_1.username1 = name1.getText();
//		Window_2.username2 = name2.getText();
//		ChatRoom.createRoom();
//	}
//}
