package experiments;

import java.io.FileNotFoundException;


import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSON {
	public static void main(String[] args) throws FileNotFoundException, ParseException {
		JSONObject jo = new JSONObject();
		jo.put("firstName", "John");
		jo.put("lastName", "Doe");

		JSONArray ja = new JSONArray();
		ja.add(jo);

		JSONObject mainObj = new JSONObject();
		mainObj.put("employees", ja);

		String joString = jo.toJSONString();
		
//		System.out.println(joString);
		
		JSONParser parser = new JSONParser(); 
		JSONObject json = (JSONObject) parser.parse(joString);
		
//		System.out.println(json.toJSONString());
	}
}
