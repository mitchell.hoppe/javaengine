package experiments.serverclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;

import projectx.engine.io.IO;

/**
 * 
 * @author Kenneth Lange and Max Medberry
 *
 *         This class is used for a user to connect to a given sever on
 *         localhost on port 4444
 */
public class Client {

	// Global variables for easy access and convenience for editing
	int port = 4444;
	String address = "localhost";
//	String address = "107.77.207.109";

	Socket socket;
	Scanner scanner;
	String username;

	public Client() {
		init();
	}

	public Client(String name) {
		init();
		username = name;
	}

	private void init() {
		try {

			socket = new Socket();
			System.out.print("Enter your username: ");
			scanner = new Scanner(System.in);
			username = scanner.nextLine();
			// Connects to address, hard coded to "localhost" in this project, and chooses
			// the correct port, hard coded to 4444.
			SocketAddress socketAddress = new InetSocketAddress(InetAddress.getByName(address), port);
			socket.connect(socketAddress);
			if (socket != null) {
				System.out.println(socket.toString());
				run();
			}

		} catch (IOException ioe) {
			System.err.println(ioe + "\nUnable to  connect to '" + address + "' server on port '" + port
					+ "' \nPlease make sure that the server is running.");
		}
	}

	/**
	 * Sends data to server
	 */
	private void run() {
		// Creates a new thread for the client to stay active to communicate with the
		// server and it's clients
		Thread t = new Thread(new ServerScanner(socket));
		t.start();

		while (true) {
			try {
				System.out.println(username + ": ");
				String message = scanner.nextLine();
				// Gets the output stream ready to be sent to.
				OutputStream out = socket.getOutputStream();

				if (message.contains("/")) {
					sendFile(message, out);
				} else {
					// Adds "['username']" to the message.
					message = username + ": " + message;

					// This is where it writes to the server.
					out.write(message.getBytes());
					if (message.contains("server.shutdown")) {
						System.err.println("You have shut down the server.");
						System.exit(-1);
					}

				}
			} catch (IOException ioe) {
				IO.printStackTrace(ioe);
			}
		}
	}

	/**
	 * This helper class sends the .txt file up to the server. File should be typed
	 * in the same as any other message, with .txt included.
	 * 
	 * @param filename Name of desired file to be sent
	 * @param out      outputstream to write file to
	 */
	private void sendFile(String message, OutputStream out) {

		String filename = message.substring(1, message.length());
		File file = new File(filename);

		try {
			FileInputStream fins = new FileInputStream(file); // creates a new input stream for the byte array to be
																// read from
			byte[] byteArray = new byte[10000];
			System.out.println("client send bytes " + fins.available());
			fins.read(byteArray); // reads file into byte array
			out.write(byteArray); // writes into output stream from byte array
			fins.close();
		} catch (FileNotFoundException e) {
			IO.printStackTrace(e);
		} catch (IOException e) {
			IO.printStackTrace(e);
		}

	}

	/**
	 * 
	 * @author Kenneth Lange
	 *
	 *         This class is directly used by the Client class for receiving the
	 *         input stream back from the server that it connects to.
	 */
	private class ServerScanner implements Runnable {
		Socket socket;

		public ServerScanner(Socket socket) {
			this.socket = socket; // Used to make the socket transferable from one class to the next.
		}

		@Override
		public void run() {
			try {
				while (true) {
					InputStream in = socket.getInputStream();
					// stream needed to be converted into a byte array.
					// checks if the stream has any "available" data to be read.
					if (in.available() > 0) {
						byte[] b = new byte[10000];
						System.out.println(in.available());
						in.read(b); // reads to the byte
						String message = new String(b); // Converts back to string then is put into users console.
						System.out.println(in.available());
						if (message.contains("/")) {
							// int spc = message.indexOf(" ");
							// File file = new File(message.substring(1, spc));
							File file = new File("attempt.txt"); // TODO change to above 2 lines

							file.createNewFile();
							// writes to newly created file
							try (FileOutputStream fos = new FileOutputStream(file)) {
								fos.write(b);
								System.out.println("file written to");

							} catch (FileNotFoundException e) {
								IO.printStackTrace(e);
							}

						} else if (message.contains(">")) {
							receiveLoc(message);
						} else {
							System.out.println(message);
							if (message.contains("server.shutdown")) {
								System.err.println("Client ended...\nServer has been shut down!");
								System.exit(-1);
							}
						}
					}
				}

			} catch (IOException ioe) {
				IO.printStackTrace(ioe);
			}
		}

	}

	public static void main(String[] args) {
		new Client();
	}

	public String isConnected() {
		if (socket == null)
			return "null";
		return "" + socket.isConnected();
	}

	public String isClosed() {
		if (socket == null)
			return "null";
		return "" + socket.isClosed();
	}

	public String printSocket() {
		if (socket == null)
			return "null";
		return socket.toString();
	}

	public float[] receiveLoc(String s) {
		s=s.substring(1);
		String[] args= s.split(",");
		float x= Float.parseFloat(args[0]);
		float y= Float.parseFloat(args[1]);
		float[] arr= {x,y};
		return arr;
	}
	
	public boolean sendBytes(byte[] packet) {
		try {
			OutputStream outputStream = socket.getOutputStream();
	
			outputStream.write(packet);
			
		} catch (IOException e) {IO.printStackTrace(e);return false;}
		return true;
	}

}