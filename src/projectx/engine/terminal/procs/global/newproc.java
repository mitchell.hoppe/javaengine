package projectx.engine.terminal.procs.global;

import java.io.*;
import java.nio.channels.FileChannel;

import projectx.engine.Handler;
import projectx.engine.io.IO;
import projectx.engine.terminal.CommandParser;
import projectx.engine.terminal.Proc;
import projectx.engine.utils.Utils;

public class newproc extends Proc{
	
	String sampleProc = "sampleproc.txt";
	String procwd;
	
	
	Handler handler;
	
	@Override
	public String run(byte[] byte_args, Handler handler) {
		String[] args = Utils.byteArrayToStringArray(byte_args, " ");
		
		if(args.length != 1) return "invalid arguments, see \'man newproc\' for list options";
		
		this.handler = handler;
		this.procwd = handler.getEngine().getTerminal().getCommandParser().PROCWD;
		
		String packagePath = args[0];
		String path = args[0].replaceAll("\\.", "/");
		String absolutePath = procwd + path + ".java";
		String name = packagePath.substring(packagePath.lastIndexOf('.') + 1, packagePath.length());
		
		if(fileExists(absolutePath) || procExists(name))
			return "error: proc already exists";
		
//		createClass(absolutePath);
//		
//		fillClass();
//		
//		registerProc(args[0]);
//		
//		createMan(path);
		return "[" + name + "] created";
	}

	private void fillClass() {
		
	}

	public boolean fileExists(String path) {
		File f = new File("src/" + path);
		return f.exists() && !f.isDirectory();
	}
	
	public boolean procExists(String name) {
		CommandParser cp = handler.getEngine().getTerminal().getCommandParser();
		return cp.getPath(name) != null;
	}
	
	public void createClass(String path) {
		try {
			File newprocFile = new File(path + ".java");
			newprocFile.createNewFile();
			FileOutputStream newprocFileOutputStream = new FileOutputStream(newprocFile);
			FileInputStream sampleProc = new FileInputStream(new File(procwd + this.sampleProc));
			FileChannel src = sampleProc.getChannel();
			FileChannel dest = newprocFileOutputStream.getChannel();
			dest.transferFrom(src, 0, src.size());
			newprocFileOutputStream.close();
			sampleProc.close();
			src.close();
			dest.close();

			IO.println("Created \'" + procwd + this.sampleProc + ".man" + "\'");
		} catch (IOException e) {IO.printStackTrace(e);}
	}
	
	public void registerProc(String localPackagePath) {
		String procregString = handler.getEngine().getTerminal().getCommandParser().PROCREGFILE;
		
		String s = localPackagePath;
		int tabs = 6 - (int)(localPackagePath.length() / 4);
		for(int i = 0; i < tabs;i++)s += "\t";
		s += "# Auto generated reg from procreg\n";
		
		Writer output;
		try {
			output = new BufferedWriter(new FileWriter(procregString, true));
			output.append(s);
			output.close();
			
			IO.println("Appended \'" + s + ".man" + "\'");
		} catch (IOException e) {IO.printStackTrace(e);}
	}
	
	public void createMan(String path) {
		CommandParser cp 	= handler.getEngine().getTerminal().getCommandParser();
		String file 		= cp.MANWD + path + ".man";
		
		try {
			File newmanFile = new File(path + ".man");
			newmanFile.createNewFile();
			FileOutputStream newprocFileOutputStream = new FileOutputStream(newmanFile);
			FileInputStream sampleProc = new FileInputStream(
					new File(handler.getEngine().getTerminal().getCommandParser().MANWD + this.sampleProc + ".man"));
			FileChannel src = sampleProc.getChannel();
			FileChannel dest = newprocFileOutputStream.getChannel();
			dest.transferFrom(src, 0, src.size());
			newprocFileOutputStream.close();
			sampleProc.close();
			src.close();
			dest.close();

			IO.println("Created \'" +
					handler.getEngine().getTerminal().getCommandParser().MANWD + this.sampleProc + ".man" + "\'");
		} catch (IOException e) {IO.printStackTrace(e);}
	}
}
